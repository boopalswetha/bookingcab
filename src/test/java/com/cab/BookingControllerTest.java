package com.cab;

import java.util.ArrayList;
import java.util.List;



import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.cab.Controller.BookingController;
import com.cab.Dto.BookingDto;
import com.cab.Dto.BookingResponseDto;
import com.cab.Dto.CancelBookingDto;
import com.cab.Dto.CancelResponse;
import com.cab.Dto.HistoryDto;
import com.cab.Dto.cabResponseDto;
import com.cab.Model.Booking;
import com.cab.Service.BookingService;

 

import junit.framework.Assert;

 

@RunWith(MockitoJUnitRunner.Silent.class)
public class BookingControllerTest {

 


    @InjectMocks
    BookingController bookingController;
    
    @Mock
    BookingService busBookingService;
    
    @Test
    public void testmakeBooingForPositive() {
        BookingResponseDto dto = new BookingResponseDto();
        BookingDto booking = new BookingDto();
        booking.setCabId( (long) 55);
        booking.setUserId((long)55);
        Mockito.when(busBookingService.makeBooking(booking)).thenReturn(dto);
        ResponseEntity<BookingResponseDto> b=bookingController.makeBooking(booking);
	
        Assert.assertEquals(dto, busBookingService.makeBooking(booking));
		Assert.assertEquals(HttpStatus.OK, b.getStatusCode());
       
        
    }
    
    @Test
    public void testmakeBookingForNegative() {
        BookingResponseDto dto = new BookingResponseDto();
        BookingDto booking = new BookingDto();
        booking.setCabId( (long) -55);
        booking.setUserId((long)55);
        Mockito.when(busBookingService.makeBooking(booking)).thenReturn(dto);
        Assert.assertEquals(dto, busBookingService.makeBooking(booking));
    }
    @Test
    public void testcancelBookingForPositive() {
        CancelBookingDto cancel = new CancelBookingDto();
        CancelResponse response = new CancelResponse();
        cancel.setBookingId((long)55);
        cancel.setUserId((long)55);
        Mockito.when(busBookingService.cancelBooking(cancel)).thenReturn(response);
        ResponseEntity<CancelResponse> b=bookingController.cancelBooking(cancel);
    	
       
        Assert.assertEquals(response, busBookingService.cancelBooking(cancel));
    }
    @Test
    public void testcancelBookingForNegative() {
        CancelBookingDto cancel = new CancelBookingDto();
        CancelResponse response = new CancelResponse();
        cancel.setBookingId((long)-55);
        cancel.setUserId((long)55);
        Mockito.when(busBookingService.cancelBooking(cancel)).thenReturn(response);
        Assert.assertEquals(response, busBookingService.cancelBooking(cancel));
    }
    @Test
    public void testgetBookingForPositive() {
        HistoryDto dto = new HistoryDto();
        CancelBookingDto cancel = new CancelBookingDto();
        Booking book = new Booking();
        List<Booking> bb= new ArrayList<Booking>();
        dto.setUserId((long)55);
        dto.setFromDate(null);
        Mockito.when(busBookingService.getBooking(null, ((long)55))).thenReturn( bb);
        ResponseEntity<List<Booking>> b=bookingController.getBooking("bb", dto.getUserId());
       
		Assert.assertEquals(HttpStatus.OK, b.getStatusCode());
       // Assert.assertEquals(book,busBookingService.getBooking( "hh", null));
        
    }
    

    @Test
    public void testmakeBookingForPositive() {
        BookingResponseDto dto = new BookingResponseDto();
        BookingDto booking = new BookingDto();
        booking.setCabId( (long) 55);
        booking.setUserId((long)55);
        Mockito.when(busBookingService.makeBooking(booking)).thenReturn(dto);
        ResponseEntity<BookingResponseDto> b=bookingController.makeBooking(booking);
	
        Assert.assertEquals(dto, busBookingService.makeBooking(booking));
		Assert.assertEquals(HttpStatus.OK, b.getStatusCode());
       
        
    }
    
    @Test
    public void testmakeookingForNegative() {
        BookingResponseDto dto = new BookingResponseDto();
        BookingDto booking = new BookingDto();
        booking.setCabId( (long) -55);
        booking.setUserId((long)55);
        Mockito.when(busBookingService.makeBooking(booking)).thenReturn(dto);
        Assert.assertEquals(dto, busBookingService.makeBooking(booking));
    }
    @Test
    public void testcancelBokingForPositive() {
        CancelBookingDto cancel = new CancelBookingDto();
        CancelResponse response = new CancelResponse();
        cancel.setBookingId((long)55);
        cancel.setUserId((long)55);
        Mockito.when(busBookingService.cancelBooking(cancel)).thenReturn(response);
        ResponseEntity<CancelResponse> b=bookingController.cancelBooking(cancel);
    	
       
        Assert.assertEquals(response, busBookingService.cancelBooking(cancel));
    }
    @Test
    public void testcancelBookinForNegative() {
        CancelBookingDto cancel = new CancelBookingDto();
        CancelResponse response = new CancelResponse();
        cancel.setBookingId((long)-55);
        cancel.setUserId((long)55);
        Mockito.when(busBookingService.cancelBooking(cancel)).thenReturn(response);
        Assert.assertEquals(response, busBookingService.cancelBooking(cancel));
    }
    @Test
    public void testgetBookingForositive() {
        HistoryDto dto = new HistoryDto();
        CancelBookingDto cancel = new CancelBookingDto();
        Booking book = new Booking();
        List<Booking> bb= new ArrayList<Booking>();
        dto.setUserId((long)55);
        dto.setFromDate(null);
        Mockito.when(busBookingService.getBooking(null, ((long)55))).thenReturn( bb);
        ResponseEntity<List<Booking>> b=bookingController.getBooking("bb", dto.getUserId());
       
		Assert.assertEquals(HttpStatus.OK, b.getStatusCode());
       // Assert.assertEquals(book,busBookingService.getBooking( "hh", null));
        
    }

}