package com.cab.Controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cab.Dto.UserDto;
import com.cab.Dto.UserRegistrationDto;
import com.cab.Service.UserService;

@RestController
public class UserController {
	
	Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	UserService userService;
	
	private UserDto userDto = new UserDto();

	@PostMapping(value = "/user")
	public ResponseEntity<Object> addUser(@RequestBody UserRegistrationDto userRegistrationDto) throws Exception {
		logger.info("Add a User for Registration");
		userDto = userService.createUser(userRegistrationDto);
		return new ResponseEntity<>(userDto, HttpStatus.OK);

	}

}
