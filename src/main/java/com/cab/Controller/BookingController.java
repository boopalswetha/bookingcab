package com.cab.Controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cab.Dto.BookingDto;
import com.cab.Dto.BookingResponseDto;

import com.cab.Dto.CancelBookingDto;
import com.cab.Dto.CancelResponse;

import com.cab.Dto.cabResponseDto;
import com.cab.Model.Booking;

import com.cab.Service.BookingService;

@RestController
public class BookingController {
	
	Logger logger = LoggerFactory.getLogger(BookingController.class);
	
	@Autowired
	BookingService bookingService;

	@PostMapping("booking")
	public ResponseEntity<BookingResponseDto> makeBooking(@RequestBody BookingDto bookingDto) {
        logger.info("Make Booking method");
		BookingResponseDto booking = bookingService.makeBooking(bookingDto);
		return new ResponseEntity<>(booking, HttpStatus.OK);

	}

	@PostMapping("cancelBooking")
	public ResponseEntity<CancelResponse> cancelBooking(@RequestBody CancelBookingDto cancelBookingDto) {
        logger.info("Cancel Booking method");
		CancelResponse booking = bookingService.cancelBooking(cancelBookingDto);
		return new ResponseEntity<>(booking, HttpStatus.OK);

	}

	@GetMapping("Booking")
	public ResponseEntity<List<Booking>> getBooking(@RequestParam String historyDto, @RequestParam Long userId) {
        logger.debug("Get booking details ========>");
		List<Booking> booking = bookingService.getBooking(historyDto, userId);
		return new ResponseEntity<>(booking, HttpStatus.OK);

	}

	@PostMapping("/cabs")
	public List<cabResponseDto> getAvaliableCabs(@RequestParam @DateTimeFormat(pattern = "dd.MM.yyyy") Date timestamp) {
		logger.debug("Get Available cabs =========>");
		return bookingService.getCabs(timestamp);
	}

	/*
	 * @PostMapping("/cab") public Cab saveCab(@RequestBody Cab cab) { return
	 * bookingService.saveCab(cab); }
	 */

}
