package com.cab.Controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cab.Dto.ResponseMessageDto;
import com.cab.Dto.UserDto;

import com.cab.Service.LoginService;


@RestController
public class LoginController {
	
	Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	LoginService loginService;
	
	@PostMapping("/login")
	public ResponseEntity<ResponseMessageDto> login(@RequestBody UserDto userDto)  {
		logger.info("User Login");
		ResponseMessageDto	responseMessageDto=	loginService.login(userDto);
		return new ResponseEntity<>(responseMessageDto, HttpStatus.OK);

	}

}