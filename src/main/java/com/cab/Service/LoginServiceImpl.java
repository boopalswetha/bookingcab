package com.cab.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cab.Dto.ResponseMessageDto;
import com.cab.Dto.UserDto;
import com.cab.Exceptions.UserNotfoundException;
import com.cab.Model.User;
import com.cab.Repository.UserRepository;

@Service
public class LoginServiceImpl implements LoginService {

	Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

	@Autowired
	UserRepository loginRepository;

	@Override
	public ResponseMessageDto login(UserDto userDto) {
		logger.info("Fing User EmailId and Password present or Not");
		User login = loginRepository.findByEmailAndPassword(userDto.getEmail(), userDto.getPassword());
		if (login == null) {
			logger.error("User is Not Exits");
			throw new UserNotfoundException("User doesnot exists");
		}
		ResponseMessageDto responseMessageDto = new ResponseMessageDto();
		responseMessageDto.setMessage("user Logged in Sucessfully");

		return responseMessageDto;

	}

}
