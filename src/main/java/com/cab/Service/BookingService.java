package com.cab.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cab.Dto.BookingDto;
import com.cab.Dto.BookingResponseDto;

import com.cab.Dto.CancelBookingDto;
import com.cab.Dto.CancelResponse;
import com.cab.Dto.HistoryDto;
import com.cab.Dto.cabResponseDto;
import com.cab.Exceptions.BookingNotFoundException;
import com.cab.Model.Booking;
import com.cab.Model.Cab;
import com.cab.Model.User;
import com.cab.Repository.BookingRepository;
import com.cab.Repository.CabRepository;
import com.cab.Repository.UserRepository;

@Service
public class BookingService {

	Logger logger = LoggerFactory.getLogger(BookingService.class);

	@Autowired
	UserRepository userRepository;
	@Autowired
	CabRepository cabRepository;
	@Autowired
	BookingRepository bookingRepository;

	public BookingResponseDto makeBooking(BookingDto bookingDto) {
		Booking booking = new Booking();
		logger.debug("Finding the CabId");
		Cab cab = cabRepository.findById(bookingDto.getCabId())
				.orElseThrow(() -> new BookingNotFoundException("cab not found"));
		logger.debug("Finding  the UserId");
		User user = userRepository.findById(bookingDto.getUserId())
				.orElseThrow(() -> new BookingNotFoundException("user not found"));

		if (cab.isCabStatus()) {
			logger.error("Cab not Availabe For Booking");
			throw new BookingNotFoundException("cab not available for booking");
		}

		cabResponseDto cabResponseDto = new cabResponseDto();
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

		LocalDateTime date = LocalDateTime.now();
		LocalDateTime date2 = cab.getTimeStamp();
		int hours = cab.getTimeStamp().getHour() - date.getHour();

		booking.setBookingTime(new Date());
		if (hours > 2 || date.compareTo(date2) < 0) {

			logger.info("Checking Hours and Date======>From Time to within 2Hours");

			cab.setCabStatus(true);

			logger.info("saving booking Order");

			cabRepository.save(cab);

			booking.setCab(cab);

			booking.setUser(user);

			booking.setBookingStatus("booked");

			String dateString = format.format(cab.getTimeStamp());
			cabResponseDto.setAvilableAt(dateString);
			BeanUtils.copyProperties(cab, cabResponseDto);
			bookingRepository.save(booking);
			BookingResponseDto bookingResponseDto = new BookingResponseDto();
			bookingResponseDto.setEmail(user.getEmail());
			bookingResponseDto.setUserName(user.getUserName());
			bookingResponseDto.setCabResponseDto(cabResponseDto);
			return bookingResponseDto;
		} else {
			logger.error("Booking order is before 2hrs");
			throw new BookingNotFoundException("you cannot book the cab before 2 hrs");
		}

	}

	public CancelResponse cancelBooking(CancelBookingDto cancelBookingDto) {
		logger.info("Finding UserId is Present or Not");
		User user = userRepository.findById(cancelBookingDto.getUserId())
				.orElseThrow(() -> new BookingNotFoundException("user not found"));
		logger.info("Finding User and BookingId Present or Not");
		Booking booking = bookingRepository.findByUserAndBookingId(user, cancelBookingDto.getBookingId());
		if (booking == null) {
			logger.error("BookingId is Not Found");
			throw new BookingNotFoundException("Booking not found");
		}
		logger.info("Finding UserId is present or Not");
		Cab cab = cabRepository.findById(booking.getCab().getCabId())
				.orElseThrow(() -> new BookingNotFoundException("cab not found"));

		LocalDateTime date = LocalDateTime.now();
		LocalDateTime date2 = cab.getTimeStamp();
		int hours = cab.getTimeStamp().getHour() - date.getHour();
		booking.setBookingTime(new Date());

		if (date.compareTo(date2) < 0) {
			if (hours < 1 || date.compareTo(date2) < 0) {
				logger.info(" Cancellation Checking For Before 1hr===>");
				cab.setCabStatus(false);
				cabRepository.save(cab);
				booking.setCab(cab);
				booking.setUser(user);
				booking.setBookingStatus("cancelled");
				logger.info("Cancel Booking Booking order save");
				bookingRepository.save(booking);
				CancelResponse cancelResponse = new CancelResponse();
				cancelResponse.setBookingId(booking.getBookingId());
				cancelResponse.setMessage("Booking cancelled sucessfully");
				return cancelResponse;
			} else {
				logger.error("You Cannot cancel the cab before 1hrs");
				throw new BookingNotFoundException("you cannot cancel the cab before 1 hr");
			}
		} else {
			logger.error("you cannot cancel the cab before dates");
			throw new BookingNotFoundException("you cannot cancel the cab before dates");
		}

	}

	public List<Booking> getBooking1(HistoryDto historyDto) {
		logger.info("Finding UserId is Present or Not");
		User user = userRepository.findById(historyDto.getUserId())
				.orElseThrow(() -> new BookingNotFoundException("user not found"));
		logger.info("find the user and stating Time to Ending Time");
		List<Booking> bookings = bookingRepository.findByUserAndBookingTimeBetween(user, historyDto.getFromDate(),
				historyDto.getToDate());
		if (bookings.isEmpty()) {
			logger.error("Booking UserId is data is Not Found");
			throw new BookingNotFoundException("bookings for particular date are not there");
		}

		return bookings;
	}

	public List<cabResponseDto> getCabs(Date timestamp) throws BookingNotFoundException {
		logger.info("Date======>" + timestamp);
		LocalDate localDate = timestamp.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		List<Cab> cabs;
		if (LocalDate.now().isEqual(localDate)) {
			logger.debug("Check local Date equal or Not");
			LocalDateTime startLocalDateTime = LocalDateTime.now();
			LocalDateTime endLocalDateTime = localDate.atTime(23, 59);
			logger.debug("Finding Strating Date to Ending Date");
			cabs = cabRepository.findAllByTimeStampBetween(startLocalDateTime, endLocalDateTime);

		} else {

			LocalDateTime startLocalDateTime = localDate.atStartOfDay();
			LocalDateTime endLocalDateTime = localDate.atTime(23, 59);
			logger.debug("Finding TimeStamp Starting Date Date to Ending Date");
			cabs = cabRepository.findAllByTimeStampBetween(startLocalDateTime, endLocalDateTime);

		}

		if (cabs.isEmpty()) {
			logger.error("Cab is Empty========>");
			throw new BookingNotFoundException("no cabs found");
		}
		List<cabResponseDto> cbrd = new ArrayList<>();
		for (Cab cab : cabs) {
			cabResponseDto cabResponseDto = new cabResponseDto();

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
			String dateString = formatter.format(cab.getTimeStamp());
			cabResponseDto.setAvilableAt(dateString);
			BeanUtils.copyProperties(cab, cabResponseDto);
			cbrd.add(cabResponseDto);

		}

		return cbrd;
	}

	public List<Booking> getBooking(String historyDto, Long userId) {
		logger.info("Check userId and month present or not");
		List<Booking> bookings = bookingRepository.findByMatchMonthAndMathDay(historyDto, userId);
		if (bookings.isEmpty()) {
			logger.error("booking Date and month is Not Present");
			throw new BookingNotFoundException("no bookings found");
		}

		return bookings;

	}

	public Cab saveCab(Cab cab) {
		logger.info("saving cab=========>");
		return cabRepository.save(cab);
	}
}
