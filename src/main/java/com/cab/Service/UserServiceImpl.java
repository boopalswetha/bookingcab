package com.cab.Service;

import java.security.SecureRandom;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cab.Dto.UserDto;
import com.cab.Dto.UserRegistrationDto;
import com.cab.Model.User;
import com.cab.Repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	UserRepository userRepository;
	User user = new User();
	private Random random = new SecureRandom();

	@Override
	public UserDto createUser(UserRegistrationDto userRegistrationDto) {

		UserDto userDto = new UserDto();
		BeanUtils.copyProperties(userRegistrationDto, user);

		int leftLimit = 97;
		int rightLimit = 122;
		int targetStringLength = 10;

		String generatedString = random.ints(leftLimit, rightLimit + 1).limit(targetStringLength)
				.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();

		user.setPassword(generatedString);
		logger.info("User Registration is Saved");
		user = userRepository.save(user);

		BeanUtils.copyProperties(user, userDto);

		return userDto;
	}
}
