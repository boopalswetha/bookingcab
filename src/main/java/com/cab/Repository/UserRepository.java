package com.cab.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cab.Model.User;

public interface UserRepository extends JpaRepository<User, Long>{

	User findByEmailAndPassword(String email, String password);

}
