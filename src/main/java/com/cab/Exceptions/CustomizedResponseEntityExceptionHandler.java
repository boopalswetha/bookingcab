package com.cab.Exceptions;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	
	/*
	 * @ExceptionHandler(DataNotFound.class) public ResponseEntity<Object>
	 * DataNotFound(DataNotFound ex, WebRequest request) {
	 * 
	 * Map<String, Object> body = new LinkedHashMap<>(); body.put("TIMESTAmp ",
	 * LocalDateTime.now()); body.put("message",
	 * "the details of bus are not correct"); body.put("status",
	 * HttpStatus.BAD_REQUEST); return new ResponseEntity<>(body,
	 * HttpStatus.BAD_REQUEST); }
	 */
	
	@ExceptionHandler(UserNotfoundException.class)
	public ResponseEntity<Object> exception(UserNotfoundException exception) {
		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", LocalDateTime.now());
		body.put("message",exception.getMessage());
		return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
	}
	
	

}
