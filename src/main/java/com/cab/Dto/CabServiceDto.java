package com.cab.Dto;

import java.util.List;

import com.cab.Model.Cab;

public class CabServiceDto {
	private List<Cab> cabs;

	public List<Cab> getCabs() {
		return cabs;
	}

	public void setCabs(List<Cab> cabs) {
		this.cabs = cabs;
	}
}
