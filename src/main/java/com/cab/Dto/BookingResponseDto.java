package com.cab.Dto;

public class BookingResponseDto {
	
	private cabResponseDto cabResponseDto;
	private String userName;
	private String email;
	public cabResponseDto getCabResponseDto() {
		return cabResponseDto;
	}
	public void setCabResponseDto(cabResponseDto cabResponseDto) {
		this.cabResponseDto = cabResponseDto;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

}
